<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email',191)->unique();
            $table->string('password')->nullable();
            $table->string('mobile_country_code')->nullable();
            $table->string('mobile')->nullable();
            $table->string('mobile_except_code')->nullable();
            $table->rememberToken();
            $table->string('api_token')->nullable();
            $table->string('session_id')->nullable();
            $table->text('twilio_id_entity')->nullable();
            $table->text('device_id')->nullable();
            $table->string('device_type')->nullable();
            $table->tinyInteger('status')->default(1)->comment('0=>Disable, 1=>Enable')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
