<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLanguagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('languages', function (Blueprint $table) {
          $table->increments('id');
          $table->string('name')->nullable();
          $table->string('code')->nullable();
          $table->tinyInteger('status')->default(1)->comment('0=>Disable, 1=>Enable')->nullable();
          $table->tinyInteger('default')->default(0)->nullable();
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('languages');
    }
}
