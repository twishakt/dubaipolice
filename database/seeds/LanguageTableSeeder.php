<?php

use Illuminate\Database\Seeder;
use App\Language;

class LanguageTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Language::create(['name'=>'English','code'=>'en','status'=>1,'default'=>1]);
        Language::create(['name'=>'Arabic','code'=>'ar','status'=>1]);
    }
}
