<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Admin;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $user = Admin::create(['name' => 'admin', 'email' => 'admin@dubaipolice.com', 'password' => '123456', 'type' => 'admin']);

        // Reset cached roles and permissions
        app()['cache']->forget('spatie.permission.cache');

        // create permissions
        Permission::create(['guard_name' => 'admin','name' => 'user_create']);
        Permission::create(['guard_name' => 'admin','name' => 'user_read']);
        Permission::create(['guard_name' => 'admin','name' => 'user_update']);
        Permission::create(['guard_name' => 'admin','name' => 'user_delete']);

        Permission::create(['guard_name' => 'admin','name' => 'role_create']);
        Permission::create(['guard_name' => 'admin','name' => 'role_read']);
        Permission::create(['guard_name' => 'admin','name' => 'role_update']);
        Permission::create(['guard_name' => 'admin','name' => 'role_delete']);

        Permission::create(['guard_name' => 'admin','name' => 'permission_create']);
        Permission::create(['guard_name' => 'admin','name' => 'permission_read']);
        Permission::create(['guard_name' => 'admin','name' => 'permission_update']);
        Permission::create(['guard_name' => 'admin','name' => 'permission_delete']);

        // create roles and assign existing permissions
        $role = Role::create(['guard_name' => 'admin','name' => 'super-admin']);
        $user->assignRole('super-admin');

        $role = Role::create(['guard_name' => 'admin','name' => 'admin']);
        $role->givePermissionTo('user_create');
        $role->givePermissionTo('user_read');
        $role->givePermissionTo('user_update');
        $role->givePermissionTo('user_delete');

        $role->givePermissionTo('role_create');
        $role->givePermissionTo('role_read');
        $role->givePermissionTo('role_update');
        $role->givePermissionTo('role_delete');
    }
}
