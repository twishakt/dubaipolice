@extends('layouts.app')

<style>
    .welcome-h3{
        text-align: center;padding-top: 30px;padding-bottom: 30px;
    }
    .officer-img{
        width: 55px;
        border-radius: 50%;
        background: #fff;
        border: 1px solid #ddd;
        padding: 10px;
        margin-top: 20px;
        margin-bottom: 20px;
    }.pt-4, .py-4 {
    padding-top: 0rem!important
}
</style>
@section('content')
<div class="container-fluid" style=" background: #717980 !important; padding-left: 0px !important; padding-right: 0px !important;overflow: hidden;">
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-12">
                <h3 style="text-align: center;padding-top: 100px;padding-bottom: 85px;font-size: 35px;font-weight: 600;color: #fff;">Dubai Police</h3>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-12 welcome-h3">
            <h3 >Welcome!</h3>
            @if(isset($user->image))
            <img src="{{url(asset('uploads/users/profiles/'.$user->image))}}" alt="Officer" class="officer-img">
            @else
            <img src="{{url(asset('images/police.jpg'))}}" alt="Officer" class="officer-img">
            @endif
            <span>{{$user->name}}</span>
            <h3 style="margin-bottom: 20px;">Choose which exhibition review you want to start</h3>
                <select class="form-control" style="margin-bottom: 20px;">
                    <option></option>
                </select>
            <input type="submit" value="Start" class="btn btn-primary">
        </div>
    </div>
</div>
@endsection

