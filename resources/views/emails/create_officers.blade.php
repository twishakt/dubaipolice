<table class="" align="center" style="border-spacing:0;color:#000000;font-family:sans-serif;margin:0 auto;max-width:600px;width:100%">
            <tbody>
            <tr>
              <td class="" style="background:#ffffff; border-bottom-color: green;border-bottom-style:solid;border-bottom-width:10px;padding:0" bgcolor="#ffffff">
                  <table width="100%" style="border-spacing:0;color:#000000;font-family:sans-serif">
                      <tbody>
                      <tr>
                          <td class="" style="padding:0 15px 15px;width:100%">
                              <a href="" style="color:#009fdf;text-decoration:none" target="_blank" data-saferedirecturl="">
                                  <h1>DubaiPolice</h1>
                              </a>
                          </td>
                      </tr>
                  </tbody></table>
              </td>
          </tr>
            <tr>
                <td class="" style="background:#ffffff;border-bottom-color:#dadada;border-bottom-style:dashed;border-bottom-width:1px;padding:0" bgcolor="#ffffff">
                    <table width="100%" style="border-spacing:0;color:#000000;font-family:sans-serif">
                        <tbody><tr>
                            <td class="" style="padding:15px;width:100%">
                            <br/>
                                <p class="" style="font-size:20px;font-weight:bold;line-height:22px;margin:0 0 15px;text-transform:none">Hello, {{$name}}</p>
                                <p style="font-size:14px;line-height:22px;margin:0 0 15px">Welcome to Dubai. Police</p>
                                <p class="" style="font-size:14px;line-height:22px;margin:0">Please find the credentials below for login to Dubai Police.</p></br>
                                <p class="" style="font-size:14px;line-height:22px;margin:0">Link: <a href="{{url('/')}}">{{url('/')}}</a></p>
                                <p class="" style="font-size:14px;line-height:22px;margin:0">Email: {{$email}}</p>
                                <p class="" style="font-size:14px;line-height:22px;margin:0">Password: {{$password}}</p>
                            </td>
                        </tr>
                    </tbody></table>
                </td>
            </tr>
            <tr>
                <td class="" style="background:#ffffff;padding:0" bgcolor="#ffffff">
                    <table width="100%" style="border-spacing:0;color:#000000;font-family:sans-serif">
                        <tbody><tr>
                            <td class="" style="padding:15px;width:100%">
                                <p class="" style="font-size:16px;font-weight:bold;line-height:22px;margin:0 0 15px;text-transform:uppercase">Need help?</p>
                                <p style="font-size:14px;line-height:22px;margin:0 0 15px">For assistance, please <a href="mailto:info@dubaipolice.com" style="color: green;text-decoration:none" target="_blank" data-saferedirecturl="">get in touch</a>.</p>
                                <p class="" style="font-size:14px;line-height:22px;margin:0">Please do not reply to this email.</p>
                            </td>
                        </tr>
                    </tbody></table>
                </td>
            </tr>

            <tr>
                <td class="" style="background:#ededed;padding:0" bgcolor="#ededed">
                    <table width="100%" style="border-spacing:0;color:#000000;font-family:sans-serif">
                        <tbody><tr>
                            <td class="" align="center" style="padding:15px;width:100%">
                                <p class="" style="font-size:12px;line-height:18px;margin:0">{{date('Y')}} &copy; Dubai Police. All rights protected.</p>
                            </td>
                        </tr>
                    </tbody></table>
                </td>
            </tr>

        </tbody></table>
