@extends('layouts.app')

@section('content')
    <div class='col-lg-4 col-lg-offset-4'>
        <h1><center>404<br>
        The webpage you were trying to reach could not be found on the server.</center></h1>
    </div>

@endsection