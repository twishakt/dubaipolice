@extends('adminlte::page')

@section('title', 'Add new Rank')

@section('content_header')
<h1><i class='fa fa-get-pocket'></i> Add Rank</h1>
@stop

@section('content')
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="box box-primary">
            <div class="box-header with-border">
                <a href="{{ url('/admin/ranks') }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
            </div>
            <div class="box-body">
               @include('admin.ranks.form')
            </div>
        </div>
    </div>
</div>
@stop
@section('css')
     <!-- iCheck for checkboxes and radio inputs -->
     <link rel="stylesheet" href="{{asset('vendor/adminlte/plugins/iCheck/all.css')}}">
     <style media="screen">
          #preview_image {
               width: 300px !important;
               height: 101px !important;
               object-fit: cover;
          }
     </style>
@stop
@section('js')
<!-- iCheck 1.0.1 -->
<script src="{{asset('vendor/adminlte/plugins/iCheck/icheck.min.js')}}"></script>
<script type="text/javascript">

     //iCheck for checkbox and radio inputs
     $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
          checkboxClass: 'icheckbox_minimal-blue',
          radioClass: 'iradio_minimal-blue'
     });

      $('#seperate').on('ifChecked', function(event){
           $("#arabic_image").show();
      });

      $('#seperate').on('ifUnchecked', function(event){
           $("#arabic_image").hide();
      });

     // function readURL(input) {
     //        if (input.files && input.files[0]) {
     //            var reader = new FileReader();
     //            reader.onload = function (e) {
     //                $('#preview_image')
     //                    .attr('src', e.target.result)
     //                    .width(300)
     //                    .height(101);
     //            };
     //            reader.readAsDataURL(input.files[0]);
     //        }
     //    }
</script>
@stop
