@extends('adminlte::page')

@section('title', 'Admin Ranks')

@section('content_header')
<h1><i class="fa fa-get-pocket"></i> Admin Ranks</h1>
@stop

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="box box-success">
            <div class="box-header">
                <?php $user = Auth::guard('admin')->user(); ?>
                @if($user->hasRole('super-admin','admin') || $user->hasPermissionTo('rank_create','admin'))
                    <a href="{{ route('admin.ranks.create') }}" class="btn btn-success btn-sm">Add Rank</a>
                @endif
            </div>
            <div class="box-body">

                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="table-id">
                        <thead>
                            <tr>
                                <th>Row No</th>
                                <th>Name</th>
                                <th>Actions</th>
                            </tr>
                        </thead>

                    </table>
                </div>

            </div>
        </div>
    </div>
</div>
@stop
@section('js')
<script src="{{asset('vendor/adminlte/plugins/iCheck/icheck.min.js')}}"></script>
<script type='text/javascript'>
$(function () {
    oTable = $('#table-id').DataTable({
        processing: false,
        serverSide: true,
        orderable: true,
        searchable: true,
        ajax: {
            url: '{!! route('admin.ranks.datatable') !!}',
            type: 'post',
            headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'}
        },
        columns: [
             {
                render: function (data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                },
                searchable: false
             },
            {data: 'name', name: 'name'},
            {data: 'actions', name: 'actions', orderable: false, searchable: false}
        ]
    });

    $('#table-id').on('click', '.destroy', function (e) {
        e.preventDefault();

        var href = $(this).attr('href');

        bootbox.confirm("{{ trans('myadmin.confirm-delete') }}", function (result) {
            if (result === false)
                return;

            $.ajax({
                url: href,
                method: 'DELETE',
                headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
                success: function () {
                    oTable.ajax.reload();
                    growl("User successfully deleted.", "success");
                }
            });
        });
    });
});
</script>
@stop
