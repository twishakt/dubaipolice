@if(isset($data))
{{ Form::model($data, array('route' => array('admin.ranks.update', $data->id), 'method' => 'PUT', 'files' => 'true')) }}
@else
{!! Form::open(
  array(
      'route' => 'admin.ranks.store',
      'class' => 'form',
      'novalidate' => 'novalidate',
      'files' => 'true'))
!!}
@endif
<div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
   {{ Form::label('name', 'Name') }}
   {{ Form::text('name', null, array('class' => 'form-control', 'autocomplete' =>'off')) }}
   {!! $errors->first('name','<p class="text-danger"><strong>:message</strong></p>') !!}
</div>

{{ Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', array('class' => 'btn btn-primary')) }}

{{ Form::close() }}
