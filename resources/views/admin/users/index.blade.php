@extends('adminlte::page')

@section('title', 'Admin Users')

@section('content_header')
<h1><i class="fa fa-users"></i> Admin Users</h1>
@stop

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="box box-success">
            <div class="box-header">
                <?php $user = Auth::guard('admin')->user(); ?>
                @if($user->hasRole('super-admin','admin') || $user->hasPermissionTo('role_read','admin'))
                <a href="{{ route('admin.roles.index') }}" class="btn btn-default btn-sm">Roles</a>
                @endif
                @if($user->hasRole('super-admin','admin') || $user->hasPermissionTo('permission_read','admin'))
                <a href="{{ route('admin.permissions.index') }}" class="btn btn-default btn-sm">Permissions</a>
                @endif
                @if($user->hasRole('super-admin','admin') || $user->hasPermissionTo('user_create','admin'))
                {{-- <a href="{{ route('admin.users.create') }}" class="btn btn-success btn-sm">Add User</a> --}}
                @endif
            </div>
            <div class="box-body">

                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="users-table">

                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Date/Time Added</th>
                                <th>User Roles</th>
                                <th>Actions</th>
                            </tr>
                        </thead>


                    </table>
                </div>

            </div>
        </div>
    </div>
</div>
@stop
@section('js')
<script src="{{asset('vendor/adminlte/plugins/iCheck/icheck.min.js')}}"></script>
<script type='text/javascript'>
$(function () {
    oTable = $('#users-table').DataTable({
        processing: false,
        serverSide: true,
        ajax: {
            url: '{!! route('admin.users.datatable') !!}',
            type: 'post',
            headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'}
        },
        columns: [
            {data: 'id', name: 'id'},
            {data: 'name', name: 'name'},
            {data: 'email', name: 'email'},
            {data: 'created_at', name: 'created_at', orderable: false, searchable: false},
            {data: 'roles', name: 'roles', orderable: false, searchable: false},
            {data: 'actions', name: 'actions', orderable: false, searchable: false}
        ]
    });

    $('#users-table').on('click', '.destroy', function (e) {
        e.preventDefault();

        var href = $(this).attr('href');

        bootbox.confirm("{{ trans('myadmin.confirm-delete') }}", function (result) {
            if (result === false)
                return;

            $.ajax({
                url: href,
                method: 'DELETE',
                headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
                success: function () {
                    oTable.ajax.reload();
                    growl("User successfully deleted.", "success");
                }
            });
        });
    });
});
</script>
@stop
