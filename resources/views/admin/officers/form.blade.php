<form id="user-form" enctype="multipart/form-data" action="@if(isset($data)) {{route('admin.officers.update', $data->id)}} @else{{route('admin.officers.store')}}@endif" method="post">
     @if(isset($data))
     <input name="_method" type="hidden" value="PUT">
     @endif
    {{ csrf_field() }}
     <div class="modal-header text-center">
         <div class="image-wrapper">
            <input type="file" name="image" id="imgupload" style="display: none">
            @if(isset($data))
            <img class="card-img-top profile-img myImg" id="OpenImgUpload" src="{{url(asset('uploads/users/profiles/'.$data->image))}}" alt="Officer">
            @else
            <img class="card-img-top profile-img myImg" id="OpenImgUpload" src="{{url(asset('images/police.jpg'))}}" alt="Officer">
            @endif
         </div>
         <p id="image" class="cls_errror validation_error" style="display:none;"></p>
     </div>
     <div class="form-group">
         <label>Officer Name</label>
         <input type="text" name="name" class="form-control" value="@if(isset($data)){{$data->user_name}}@endif">
         <p id="name" class="cls_errror validation_error" style="display:none;"></p>
     </div>
     <div class="form-group">
         <label>Officer Rank</label>
         <select class="form-control" name="rank">
            <option value="">Select a Rank</option>
            @foreach($getRanks as $rank)
            <option @if(isset($data)) @if($data->rank_id == $rank->id) selected @endif @endif value="{{$rank->id}}">{{$rank->name}}</option>
            @endforeach
         </select>
         <p id="rank" class="cls_errror validation_error" style="display:none;"></p>
     </div>
     <div class="form-group">
         <label>Officer Email</label>
         <input type="email" name="email" class="form-control" value="@if(isset($data)){{$data->email}}@endif">
         <p id="email" class="cls_errror validation_error" style="display:none"></p>
     </div>
     <div class="form-group">
         <label>Officer Phone</label>
         <input type="text" name="phone" class="form-control" value="@if(isset($data)){{$data->mobile}}@endif">
         <p id="phone" class="cls_errror validation_error" style="display:none"></p>
     </div>
 <div class="modal-footer d-flex justify-content-center">
     <button class="btn btn-danger" data-dismiss="modal">Cancel</button>
     <button type="submit" name="button" class="btn btn-success btn-add">@if(isset($data)) Update @else Add @endif</button>
 </div>
</form>
