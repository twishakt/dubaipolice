
<div class="col-md-4 ">
    <div class="btn-group" style="float: right;padding-top: 20px;margin-right: 20px;">

        <!-- Basic dropdown -->
        <a data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="font-size: 20px;color: black"><i class="fa fa-fw fa-ellipsis-v"></i></a>

        <div class="dropdown-menu" style="">
            <li class="dropdown-lists">Edit</li>
            <li class="dropdown-lists">Delete</li>
            <li class="dropdown-lists">Assign</li>
        </div>
        <!-- Basic dropdown -->

    </div>
    <div class="card card-background" style="text-align: center">

        <img class="card-img-top profile-img" src="{{url(asset('images/police.jpg'))}}" alt="Officer">
        <div class="card-body">
            <h3 class="card-title">Ubaid Nk</h3>
            <h5 class="card-title">Rank</h5>
            <h4 class="card-text">Exhibition</h4>
            <p>International K9 Summit Dubai</p>
            <p>Phone:1234444</p>
            <p>Email:officer@gmail.com</p>
        </div>
    </div>
</div>
