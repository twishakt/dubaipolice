@extends('adminlte::page')

@section('title', 'Officer')

@section('content_header')
<h1><i class="fa fa-get-pocket"></i> Officers</h1>
@stop
@section('content')
<div class="row">

    <div class="col-md-12">
        <div class="show-users">

        </div>
        @if(!$officers->isEmpty())
        @foreach($officers as $officer)
        <div class="col-md-4 officer_block">
            <div class="card card-background officers-lists">
                 <div class="manage-btn">
                      <ul class="menu">
                           <li class="expanded">
                                <a href="#"><i class="fa fa-ellipsis-v"></i></a>
                                <ul class="sub-menu">
                                     <li><a href="#"  data-toggle="modal" data-target="#modalOfficerCreate"  data-href="{{route('admin.officers.edit', $officer->id)}}">Edit</i></a></li>
                                     <li><a href="{{route('admin.officers.destroy', $officer->id)}}" class="destroy">Delete</a></li>
                                     <li><a href="#">Assign</a></li>
                                </ul>
                           </li>
                      </ul>

                 </div>
                <img class="card-img-top profile-img" src="{{url(asset('uploads/users/profiles/'.$officer->image))}}" alt="Officer">
                <div class="officer-datas">
                     <h2>{{$officer->officerTranslation[0]->name}}</h2>
                     <h3>{{$officer->officerRank[0]->name}}</h3>
                     <p class="phone">Phone: {{$officer->mobile}}</p>
                     <p class="email">Phone: {{$officer->email}}</p>
                </div>
            </div>
        </div>
        @endforeach
        @endif
        <div class="col-md-4">
            <div class="card card-background" style="text-align: center;padding: 90px;">
                <img class="card-img-top profile-img" src="{{url(asset('images/police.jpg'))}}" alt="Officer">
                <div class="card-body" style="margin-top: 20px;padding-bottom: 50px;" data-toggle="modal" data-target="#modalOfficerCreate" data-href="{{route('admin.officers.create')}}">
                    <div class="add-button">
                        <i class="fa fa-fw fa-plus add-icons"></i>
                    </div>
                    <p class="add-p" >Add Officer</p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modalOfficerCreate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog" role="document" style="width: 300px;">
        <div class="modal-content">

            <div class="modal-body col-md-12">

            </div>
        </div>
    </div>
</div>
<div class="loading" style="display: none;">Loading&#8230;</div>
@stop
@section('css')
     <link rel="stylesheet" href="{{asset('css/loader.css')}}">
     <style media="screen">
          .officers-lists {
               background: #fff;
               padding: 35px;
               border: 3px solid #ddd;
               text-align: center;
               position: relative;
               margin-bottom: 30px;
          }
          .phone, .email {
               text-align: left;
          }
          .manage-btn {
               position: absolute;
               top: 10px;
               right: 20px;
               font-size: 25px;
          }
          ul.menu li {
               list-style: none;
          }
          ul.menu li.expanded {
               list-style: none;
               position: relative;
          }
          .manage-btn ul.sub-menu {
               border: 1px solid red;
               position: absolute;
               right: 10px;
               top: 10px;
               background: #FFF;
               border:  1px solid #333;
               padding: 0;
               display: none;
          }
          .manage-btn ul.sub-menu li {
               border-bottom: 1px solid #333;
               width: 130px;
               line-height: 1;
               padding-bottom: 10px;
          }
          .manage-btn ul.sub-menu li:last-child {
               border-bottom: none;
               width: 130px;
          }
          .manage-btn ul.sub-menu li a {
               font-size: 13px;
               color: #333;
          }
          .modal-body {
               background: #FFF;
          }
     </style>
@stop
@section('js')
<script src="{{asset('vendor/adminlte/plugins/iCheck/icheck.min.js')}}"></script>
<script type='text/javascript'>
$(document).ready(function () {
     $("#modalOfficerCreate").on("click", "#OpenImgUpload", function(){
        $('#imgupload').trigger('click');
    });

    $('#modalOfficerCreate').on('show.bs.modal', function (e) {
         $('.loading').show();
        var url = $(e.relatedTarget).attr('data-href');
        $.ajax({
            url: url,
            context: this,
            method: "GET",
            dataType: "html",
            beforeSend: function () {
                //$('.ajax-loading').show();
            },
            success: function (response) {
                $(this).find('.modal-body').html(response);
                $('.loading').hide();
                // $('.loading').hide();
            },
            complete: function () {
                 $('.loading').hide();
                //$('.ajax-loading').hide();
            }
        });
    });

    $(document).on("submit", "form", function (e) {
        $('.loading').show();
        e.preventDefault();

        $this = this;
        var form = new FormData($this);
        // var image = $('#imgupload').prop('files')[0];
        // console.log(image);
        // form.append('image', image);
        var url = $(this).attr('action');
        $.ajax({
            url: url,
            processData: false,
            contentType: false,
            type: 'POST',
            headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
            data: form,
            success: function (responce) {
                if (responce.status == "100") {
                    $(".cls_errror").css("display", 'none');
                    $.each(responce.errors, function (key, value) {
                        $('#' + key).css("display", '').html(value);
                    });
                     $('.loading').hide();
                }
                if (responce.status == "200") {
                      $('.loading').hide();
                     $('#modalOfficerCreate').modal('hide');
                    growl(responce.message, "danger");
                }
                if (responce.status == "400") {
                     $('#modalOfficerCreate').modal('hide');
                      $('.loading').hide();
                    growl(responce.message, "success");
                    location.reload();
                }
            }
        });
    });
});
function imageIsLoaded(e) {
    $('.myImg').attr('src', e.target.result);
}
;
function getFile(filePath) {
    return filePath.substr(filePath.lastIndexOf('\\') + 1).split('.')[0];
}


$(":file").change(function () {
    if (this.files && this.files[0]) {
        var reader = new FileReader();
        reader.onload = imageIsLoaded;
        reader.readAsDataURL(this.files[0]);
    }
});

$('.menu').children().click(function(){
    event.preventDefault();
    $(this).children('.sub-menu').slideToggle('slow');
});

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#OpenImgUpload').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}

$("#modalOfficerCreate").on("change", "#imgupload", function(){
    readURL(this);
});

$('.menu').on('click', '.destroy', function (e) {
   e.preventDefault();
   var outer = $(this).parent().parent().parent().parent().parent().parent().parent();
   var href = $(this).attr('href');
   bootbox.confirm("Do you want to delete this one", function (result) {
        if (result === false)
            return;

        $.ajax({
            url: href,
            method: 'DELETE',
            headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
            success: function () {
                 outer.hide();
                 growl("Officer successfully deleted.", "success");
            }
        });
   });
});

</script>
@stop
