@extends('adminlte::page')

@section('title', 'Roles')

@section('content_header')
<h1><i class="fa fa-key"></i> Roles
    @stop

    @section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-success">
                <div class="box-header">
                    <?php $user = Auth::guard('admin')->user(); ?>
                    @if($user->hasRole('super-admin','admin') || $user->hasPermissionTo('user_read','admin'))
                    <a href="{{ route('admin.users.index') }}" class="btn btn-default pull-right btn-sm">Users</a>
                    @endif
                    @if($user->hasRole('super-admin','admin') || $user->hasPermissionTo('permission_read','admin'))
                    <a href="{{ route('admin.permissions.index') }}" class="btn btn-default pull-right btn-sm">Permissions</a>
                    @endif
                    @if($user->hasRole('super-admin','admin') || $user->hasPermissionTo('role_create','admin'))
                    <a href="{{ URL::to('admin/roles/create') }}" class="btn btn-success pull-right btn-sm">Add Role</a>
                    @endif
                </div>
                <div class="box-body">

                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover" id="role-table">
                            <thead>
                                <tr>
                                    <th>Role</th>
                                    <th>Permissions</th>
                                    <th>Operation</th>
                                </tr>
                            </thead>

                            <tbody>
                                @foreach ($roles as $role)
                                <tr>

                                    <td>{{ $role->name }}</td>

                                    <td style="width: 70%;">
                                        {!! '<span class="label label-outlined">'.implode('</span> <span class="label label-outlined">', $role->permissions()->pluck('name')->toArray()).'</span>' !!}
                                    </td>
                                    <td style="width: 10%;">
                                        @if($user->hasRole('super-admin','admin') || $user->hasPermissionTo('role_update','admin'))
                                        <a href="{{ URL::to('admin/roles/'.$role->id.'/edit') }}" class="btn btn-xs btn-info pull-left" style="margin-right: 3px;">Edit</a>
                                        @endif
                                        @if($user->hasRole('super-admin','admin') || $user->hasPermissionTo('role_delete','admin'))
                                        {!! Form::open(['method' => 'DELETE', 'route' => ['admin.roles.destroy', $role->id] ]) !!}
                                        {!! Form::submit('Delete', ['class' => 'btn btn-xs btn-danger destroy']) !!}
                                        {!! Form::close() !!}
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>

                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
    @stop
    @section('js')
    <script type='text/javascript'>
        $(function () {
            $('#role-table').on('click', '.destroy', function (e) {
                e.preventDefault();
                var form = $(this);
                bootbox.confirm("{{ trans('myadmin.confirm-delete') }}", function (result) {
                    if (result === false) {
                        return;
                    } else {
                        form.parent().submit();
                    }
                });
            });
        });
    </script>    
    @stop