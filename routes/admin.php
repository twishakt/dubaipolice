<?php

/*
  |--------------------------------------------------------------------------
  | Admin Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register Admin routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

Route::group(['namespace' => 'Auth', 'as' => 'admin.'], function () {
    Route::get('/login', 'AdminLoginController@showLoginForm')->name('login');
    Route::post('/login', 'AdminLoginController@login')->name('login.submit');
    Route::post('/logout', 'AdminLoginController@logout')->name('logout');

    //admin password reset routes
    Route::post('/password/email', 'AdminForgotPasswordController@sendResetLinkEmail')->name('password.email');
    Route::get('/password/reset', 'AdminForgotPasswordController@showLinkRequestForm')->name('password.request');
    Route::post('/password/reset', 'AdminResetPasswordController@reset');
    Route::get('/password/reset/{token}', 'AdminResetPasswordController@showResetForm')->name('password.reset');
});

Route::group(['middleware' => 'isAdmin', 'namespace' => 'Admin', 'as' => 'admin.'], function () {
    Route::get('/', 'HomeController@index')->name('dashboard');
    Route::resource('users', 'AdminController');
    Route::any('users/dt', 'AdminController@datatable')->name('users.datatable');
    Route::get('user/profile', 'AdminController@profile')->name('user.profile');
    Route::post('user/profile', 'AdminController@profilePost')->name('user.profile.post');
    Route::post('user/profile/avatardelete', 'AdminController@avatarDelete')->name('user.avatardelete');


    Route::post('changeAdminLanguage', 'HomeController@changeAdminLanguage')->name('language.change');

    Route::resource('roles', 'RoleController');
    Route::resource('permissions', 'PermissionController');

    Route::resource('ranks', 'RankController');
    Route::any('ranks/dt', 'RankController@datatable')->name('ranks.datatable');

    Route::resource('officers', 'OfficerController');

    // Route::get('officers', 'OfficerController@index')->name('officer.index');
    // Route::post('officers/store', 'OfficerController@store')->name('officer.store');
    // Route::post('officers-update/{id?}', 'OfficerController@officerUpdate')->name('officer.update');

    // Route::get('form-load', ['as' => 'officer-form-load', 'uses' => 'OfficerController@formLoad']);
    // Route::get('form-load-edit/{id?}', ['as' => 'officer-form-load-edit', 'uses' => 'OfficerController@formLoadEdit']);


});
