<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::group(['middleware' => 'webApi'], function () {
    Route::post('login', 'Api\V1\UserController@login');
    Route::post('forgotPassword', 'Api\V1\PasswordController@getResetToken');
});


Route::group(['middleware' => 'isApiUser', 'namespace' => 'Api\V1'], function () {

});
