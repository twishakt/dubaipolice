<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rank extends Model
{
    protected $table = 'ranks';

    public function rankTranslation() {
       return $this->hasMany(RankTranslation::class, 'rank_id', 'id');
   }
}
