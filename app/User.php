<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function officerTranslation() {
       return $this->hasMany(UserTranslation::class, 'user_id', 'id');
   }

   public function officerRank() {
     return $this->hasMany(RankTranslation::class, 'rank_id', 'rank_id');
 }
}
