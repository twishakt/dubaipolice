<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;
use App\Notifications\AdminResetPasswordNotification;

class Admin extends Authenticatable {

    use Notifiable;
    use HasRoles;

    protected $guard = 'admin';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'admin_id', 'name', 'email', 'password', 'status', 'type', 'last_login'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function setPasswordAttribute($password) {
        $this->attributes['password'] = bcrypt($password);
    }

    /**
     * Check if current user has an avatar
     *
     * @return string|false
     */
    public function getAvatarPathAttribute() {
        return public_path('images/avatars/' . md5($this->id . $this->created_at) . '.jpg');
    }

    /**
     * Return current user avatar uri
     *
     * @return string
     */
    public function getAvatarUrlAttribute() {
        if (is_file($this->avatar_path)) {
            $ts = filemtime($this->avatar_path);
            return asset('images/avatars/' . md5($this->id . $this->created_at) . '.jpg?t=' . $ts);
        }
        return asset("/images/default_user.png");
    }

    public function sendPasswordResetNotification($token) {
        $this->notify(new AdminResetPasswordNotification($token));
    }

}
