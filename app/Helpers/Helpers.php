<?php

function recurse($arr, $level = 1, $pid = 0) {
    # we have a numerically-indexed array. go through each item:
    foreach ($arr as $n) {
        $selOption = ($pid == $n['id']) ? ' selected' : '';
        # print out the item ID and the item name
        echo '<option value="' . $n['id'] . '"' . $selOption . '>'
        . str_repeat("-", $level)
        . $n['name']
        . '</option>'
        . PHP_EOL;
        # if item['children'] is set, we have a nested data structure, so
        # call recurse on it.
        if (isset($n['children'])) {
            # we have children: RECURSE!!
            recurse($n['children'], $level + 1, $pid);
        }
    }
}

function uniqueString($table, $col, $chars = 7, $prefix = '') {

    $unique = false;

    // Store tested results in array to not test them again
    $tested = [];

    do {

        if (!empty($prefix)) {
            $chars = $chars - strlen($prefix);
            $random = $prefix . str_random($chars);
        } else {
            // Generate random string of characters
            $random = str_random($chars);
        }

        // Check if it's already testing
        // If so, don't query the database again
        if (in_array($random, $tested)) {
            continue;
        }

        // Check if it is unique in the database
        $count = DB::table($table)->where($col, '=', $random)->count();

        // Store the random character in the tested array
        // To keep track which ones are already tested
        $tested[] = $random;

        // String appears to be unique
        if ($count == 0) {
            // Set unique to true to break the loop
            $unique = true;
        }

        // If unique is still false at this point
        // it will just repeat all the steps until
        // it has generated a random string of characters
    } while (!$unique);


    return $random;
}

function getdefaultLanguage() {
    $defaultLanguage = DB::table('languages');
    if (session()->has('adminLanguage')) {
        $defaultLanguage = $defaultLanguage->where('id', session()->get('adminLanguage'));
    } else {
        $defaultLanguage = $defaultLanguage->where('default', 1);
    }
    $defaultLanguage = $defaultLanguage->first();
    return $defaultLanguage;
}

function getCurrentLanguage() {
    $language = DB::table('languages')->where('code', App::getLocale())->first();
    return $language;
}
