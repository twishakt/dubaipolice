<?php

namespace App\Http\Middleware;

use Closure;
use App\Http\Controllers\ApiController;
use Illuminate\Support\Facades\Auth;

class RedirectIfNotApiValidated {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null) {
        $api = new ApiController();
        $language = $api->setApiLanguage($request->header('language', 'en'));

        if (!Auth::guard('api')->check()) {
            if ($request->ajax() || $request->wantsJson() || $guard == "api") {
                return $api->errorResponse(trans('api.unauthorized'));
            }
        }
        if (Auth::guard('api')->check()) {
            if (env('APP_ENV') == 'production') {
                $sessionId = $request->header('sessionId');
                $user = Auth::guard('api')->user();
                if ($user->session_id != $sessionId) {
                    return $api->sessionOutResponse(trans('api.user_invalid_session'));
                }
            }
            return $next($request);
        }

        return $api->errorResponse(trans('api.user_not_exists'));
    }

}
