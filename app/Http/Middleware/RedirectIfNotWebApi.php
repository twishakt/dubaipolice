<?php

namespace App\Http\Middleware;

use Closure;
use App\Http\Controllers\ApiController;
use Illuminate\Support\Facades\Auth;

class RedirectIfNotWebApi {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = '') {
        $api = new ApiController();
        $language = $api->setApiLanguage($request->header('language', 'en'));
        if ($request->hasHeader('Authorization')) {
            $token = $request->bearerToken();
            if (Auth::guard('api')->check()) {
                if (env('APP_ENV') == 'production') {
                    $sessionId = $request->header('sessionId');
                    $user = Auth::guard('api')->user();
                    if($user->session_id != $sessionId)
                    {
                    return $api->sessionOutResponse(trans('api.user_invalid_session'));
                    }
                }
            } elseif ($token != env('API_TOKEN')) {
                return $api->errorResponse(trans('api.token_error'));
            }

            return $next($request);
        } else {
            return $api->errorResponse(trans('api.api_validate'));
        }
    }

}
