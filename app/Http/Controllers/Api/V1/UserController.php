<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use Auth;
use Validator;
use Hash;
use App\User;

class UserController extends ApiController
{
     public function __construct() {
        $this->middleware(['api']);
    }

    public function login(Request $request) {
        $validator = Validator::make($request->all(), [
                    'email' => 'required',
                    'password' => 'required',
        ]);

        if ($validator->fails()) {
            $errorMessage = $validator->messages();
            return $this->errorResponse(trans('api.error_required_fields'), $errorMessage);
        }

        $exist = User::where('email', $request->email)->first();
        if($exist) {
             if (Hash::check($request->password, $exist->password) == true) {
                  $sessionId = $this->generateSessionId();
                  $this->updateSession($sessionId, $exist->id);

                  $user = User::where('email', $exist->email)->first();

                  $user['image'] = asset('uploads/users/profiles/' . $user->image);

                  return $this->successResponse(trans('api.login_success'), $user);
             } else {
                  return $this->errorResponse(trans('api.user.invalid_credentials'));
             }
        } else {
             return $this->errorResponse(trans('api.user.invalid_credentials'));
        }
    }

    public function generateSessionId()
    {
        return uniqid() . time();
    }

    private function updateSession($sessionId, $userId)
    {
        User::where('id', $userId)->update(['session_id' => $sessionId]);
        return true;
    }
}
