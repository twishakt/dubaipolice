<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class OfficerController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = \App\User::join("user_translations as ut","ut.user_id","=","users.id")
                ->where("users.id",Auth::guard("web")->user()->id)
                ->where("ut.language_id",getdefaultLanguage()->id)
                ->select("ut.name","users.image")
                ->first();
        return view('dashboard.home', compact("user"));
    }
}
