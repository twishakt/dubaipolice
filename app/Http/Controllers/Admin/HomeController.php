<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function changeAdminLanguage(Request $request) {
         if($request) {
              $request->session()->put('adminLanguage', $request->adminLanguage);
               return redirect('admin') ->with('growl', ['Language successfully updated.', 'success']);;
         }
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.home');
    }
}
