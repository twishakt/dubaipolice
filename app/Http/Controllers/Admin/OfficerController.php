<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use App\Language;
use App\Rank;
use Hash;
use Mail;
use App\RankTranslation;
use URL;
use Auth;
use App\User;
use App\UserTranslation;
use App\Traits\ImageTraits;
use File;
use Storage;

class OfficerController extends Controller
{
        use ImageTraits;

        public function __construct()
        {
            $this->middleware(function ($request, $next) {
                $user = Auth::guard('admin')->user();
                if ($user->hasRole('super-admin', 'admin') || $user->hasPermissionTo('officers_read', 'admin')) {
                    return $next($request);
                } else {
                    return redirect()->route('admin.dashboard')
                                    ->with('growl', ['Your don\'t have permission to acces this page.', 'danger']);
                }
            }, ['only' => ['index', 'profile', 'show']]);

            $this->middleware(function ($request, $next) {
                $user = Auth::guard('admin')->user();
                if ($user->hasRole('super-admin', 'admin') || $user->hasPermissionTo('officers_create', 'admin')) {
                    return $next($request);
                } else {
                    return redirect()->route('admin.dashboard')
                                    ->with('growl', ['Your don\'t have permission to acces this page.', 'danger']);
                }
            }, ['only' => ['create', 'store']]);

            $this->middleware(function ($request, $next) {
                $user = Auth::guard('admin')->user();
                if ($user->hasRole('super-admin', 'admin') || $user->hasPermissionTo('officers_update', 'admin')) {
                    return $next($request);
                } else {
                    return redirect()->route('admin.dashboard')
                                    ->with('growl', ['Your don\'t have permission to acces this page.', 'danger']);
                }
            }, ['only' => ['edit', 'update']]);

            $this->middleware(function ($request, $next) {
                $user = Auth::guard('admin')->user();
                if ($user->hasRole('super-admin', 'admin') || $user->hasPermissionTo('officers_delete', 'admin')) {
                    return $next($request);
                } else {
                    return redirect()->route('admin.dashboard')
                                    ->with('growl', ['Your don\'t have permission to acces this page.', 'danger']);
                }
            }, ['only' => ['destroy']]);
        }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $getRanks = Rank::Join("rank_translations as rt", "rt.rank_id", "=", "ranks.id")->where("rt.language_id", getdefaultLanguage()->id)->select("ranks.id", "rt.name")->get();
         $officers = User::with(['officerTranslation' => function ($query) { $query->where('language_id', getdefaultLanguage()->id);}, 'officerRank' => function ($query) { $query->where('language_id', getdefaultLanguage()->id);}])
                                   ->where('status', 1)->orderBy('id', 'desc')->get();
         return view('admin.officers.index', compact("getRanks", "officers"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         $getRanks = Rank::join("rank_translations as rt", "rt.rank_id", "=", "ranks.id")->where("rt.language_id", getdefaultLanguage()->id)->select("ranks.id", "rt.name")->get();
         return view('admin.officers.form', compact('getRanks'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $validator = \Validator::make($request->all(), [
                    'name'    => 'required',
                    'rank'    => 'required',
                    'email'   => 'required|email|unique:users',
                    'phone'   => 'required|numeric|unique:users,mobile',
                    'image' => 'required|mimes:jpeg,png,jpg|max:2048',
                        ], [
                    "image.mimes" => "The valid image type jpeg,png,jpg",
                    "image.max"   => "Image size must be less than 2MB"
        ]);
        if ($validator->fails()) {
            $errors = array_combine($validator->errors()->keys(), $validator->errors()->all());
            return response()->json(["status" => 100,'errors' => $errors]);
        }

        if (getdefaultLanguage()->id == 2) {
            return response()->json(["status" => 200,'message' => 'You can not create arabic content first']);
        }

        $password = str_random(8);

        $user = new \App\User();
        $user->email = $request->email;
        $user->password = Hash::make($password);
        $user->mobile = $request->phone;
        $user->rank_id = $request->rank;
        $user->api_token =  $this->generateApiToken();
        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $path = 'users/profiles/';
            $resizedImage = $this->resize($image, $path, 'officer_');
            if ($resizedImage) {
                $user->image = $resizedImage;
            }
        }
        $user->save();
        $languages = Language::all();
        foreach($languages as $lang){
            $userTraslators = new \App\UserTranslation();
            $userTraslators->name = $request->name;
            $userTraslators->user_id = $user->id;
            $userTraslators->language_id = $lang->id;
            $userTraslators->save();
        }
        try {
            $data = array('name' => $request->name, 'email' => $request->email, 'password' =>$password);
            Mail::send('emails.create_officers', $data, function($message) use ($request) {
                                       $message->from('admin@dubaipolice.com');
                                       $message->to($request->email)
                                       ->subject('Welcome Mail');
                          });
        } catch (\Exception $e) {

        }
        // $officer = User::with(['officerTranslation' => function ($query) { $query->where('language_id', getdefaultLanguage()->id);}, 'officerRank' => function ($query) { $query->where('language_id', getdefaultLanguage()->id);}])
        //                            ->where('status', 1)
        //                            ->where('id', $user->id)
        //                            ->orderBy('id', 'desc')->first();
        return response()->json(["status" => 400,'message' => 'Officer added successfully.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $getRanks = Rank::join("rank_translations as rt", "rt.rank_id", "=", "ranks.id")->where("rt.language_id", getdefaultLanguage()->id)->select("ranks.id", "rt.name")->get();
         $data = User::Join("user_translations", "user_translations.user_id", "=", "users.id")
                             ->Join("rank_translations", "rank_translations.rank_id", "=", "users.rank_id")
                             ->where('users.id', $id)
                             ->where('user_translations.language_id', getdefaultLanguage()->id)
                             ->select('rank_translations.name as rank_name', 'user_translations.name as user_name', 'users.mobile', 'users.email', 'users.rank_id', 'users.image', 'users.id')
                             ->first();
          return view('admin.officers.form', compact('data', 'getRanks'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $validator = \Validator::make($request->all(), [
                   'name'    => 'required',
                   'rank'    => 'required',
                   'email'   => 'required|email|unique:users,email,'.$id,
                   'phone'   => 'required|numeric|unique:users,mobile,'.$id,
                   'image' => 'mimes:jpeg,png,jpg|max:2048',
                       ], [
                   "image.mimes" => "The valid image type jpeg,png,jpg",
                   "image.max"   => "Image size must be less than 2 MB"
       ]);
       if ($validator->fails()) {
           $errors = array_combine($validator->errors()->keys(), $validator->errors()->all());
           return response()->json(["status" => 100,'errors' => $errors]);
       }
       $user = User::find($id);
       $user->email = $request->email;
       $user->mobile = $request->phone;
       $user->rank_id = $request->rank;

       if($request->hasFile('image')) {
           $image = $request->file('image');
           $path = 'users/profiles/';
           $resizedImage = $this->resize($image, $path, 'officer_');
           if ($resizedImage) {
               if (!env('CDN_ENABLED', false)) {
                   File::delete(public_path('uploads/' . $path) . $user->image);
               } else {
                   Storage::disk('s3')->delete(env('CDN_FILE_DIR', 'dev/path/uploads/') . $path . $user->image);
               }
               $user->image = $resizedImage;
           }
       }
       $user->save();

       $edit  = UserTranslation::where('user_id', $id)->where('language_id', getdefaultLanguage()->id)->first();
       $edit->name = $request->name;
       $edit->save();
       return response()->json(["status" => 400,'message' => 'Officer updated successfully.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $path = 'users/profiles/';
        if (!env('CDN_ENABLED', false)) {
            File::delete(public_path('uploads/' . $path) . $user->image);
        } else {
            Storage::disk('s3')->delete(env('CDN_FILE_DIR', 'dev/path/uploads/') . $path . $user->image);
        }
        User::destroy($id);
    }

    public function generateApiToken()
    {
        do {
            $number = str_random(60);
        } while (!empty(User::where('api_token', $number)->first(['api_token'])));

        return $number;
    }
}
