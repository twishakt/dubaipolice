<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use App\Language;
use App\Rank;
use App\RankTranslation;
use URL;
use Auth;

class RankController extends Controller {

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $user = Auth::guard('admin')->user();
            if ($user->hasRole('super-admin', 'admin') || $user->hasPermissionTo('ranks_read', 'admin')) {
                return $next($request);
            } else {
                return redirect()->route('admin.dashboard')
                                ->with('growl', ['Your don\'t have permission to acces this page.', 'danger']);
            }
        }, ['only' => ['index', 'profile', 'show']]);

        $this->middleware(function ($request, $next) {
            $user = Auth::guard('admin')->user();
            if ($user->hasRole('super-admin', 'admin') || $user->hasPermissionTo('ranks_create', 'admin')) {
                return $next($request);
            } else {
                return redirect()->route('admin.dashboard')
                                ->with('growl', ['Your don\'t have permission to acces this page.', 'danger']);
            }
        }, ['only' => ['create', 'store']]);

        $this->middleware(function ($request, $next) {
            $user = Auth::guard('admin')->user();
            if ($user->hasRole('super-admin', 'admin') || $user->hasPermissionTo('ranks_update', 'admin')) {
                return $next($request);
            } else {
                return redirect()->route('admin.dashboard')
                                ->with('growl', ['Your don\'t have permission to acces this page.', 'danger']);
            }
        }, ['only' => ['edit', 'update']]);

        $this->middleware(function ($request, $next) {
            $user = Auth::guard('admin')->user();
            if ($user->hasRole('super-admin', 'admin') || $user->hasPermissionTo('ranks_delete', 'admin')) {
                return $next($request);
            } else {
                return redirect()->route('admin.dashboard')
                                ->with('growl', ['Your don\'t have permission to acces this page.', 'danger']);
            }
        }, ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.ranks.index');
    }

    public function datatable()
    {
        $currentUser  = Auth::guard('admin')->user();
        $isSuperAdmin = $currentUser->hasRole('super-admin', 'admin');
        $datas        = Rank::Join('rank_translations', 'ranks.id', '=', 'rank_translations.rank_id')
                        ->where('rank_translations.language_id', getdefaultLanguage()->id)
                        ->select('ranks.id', 'rank_translations.name')->orderBy('ranks.id', 'asc')->get();
        return Datatables::of($datas, $isSuperAdmin, $currentUser)
                        ->rawColumns(['actions'])
                        ->editColumn('actions', function ($datas) use ($currentUser, $isSuperAdmin) {
                            $b = '';
                            if ($isSuperAdmin || $currentUser->hasPermissionTo('ranks_update', 'admin')) {
                                $b .= '<a href="' . URL::route('admin.ranks.edit', $datas->id) . '" class="btn btn-primary btn-xs mrs"><i class="fa fa-pencil"></i></a>';
                            }
                            if ($isSuperAdmin || $currentUser->hasPermissionTo('ranks_delete', 'admin')) {
                                $b .= ' <a href="' . URL::route('admin.ranks.destroy', $datas->id) . '" class="btn btn-danger btn-xs destroy"><i class="fa fa-trash"></i></a>';
                            }
                            return $b;
                        })->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.ranks.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:120',
        ]);

        if (getdefaultLanguage()->id == 2) {
            return redirect()->back()->with('growl', ['You can not create arabic content first.', 'danger'])->withInput();
        }

        $exist = Rank::join('rank_translations', 'ranks.id', '=', 'rank_translations.rank_id')
                        ->where('rank_translations.name', $request->name)->first();

        if ($exist) {
            return redirect()->back()->with('growl', ['The same rank already exist.', 'danger'])->withInput();
        }

        $data      = new Rank();
        $data->save();
        $languages = Language::all();
        foreach ($languages as $language) {
            RankTranslation::create(
                    [
                        'language_id' => $language->id,
                        'rank_id'     => $data->id,
                        'name'        => $request->name
                    ]
            );
        }

        //Redirect to the users.index view and display message
        return redirect()->route('admin.ranks.index')->with('growl', ['Rank successfully added.', 'success']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Rank::Join('rank_translations', 'ranks.id', '=', 'rank_translations.rank_id')
                        ->where('rank_translations.language_id', getdefaultLanguage()->id)
                        ->where('ranks.id', $id)
                        ->select('rank_translations.name', 'ranks.id')->first();
        return view('admin.ranks.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|max:120'
        ]);

        $languages = Language::get();

        $exist = Rank::Join('rank_translations', 'ranks.id', '=', 'rank_translations.rank_id')
                        ->where('ranks.id', '!=', $id)
                        ->where('rank_translations.name', $request->name)->first();
        if ($exist) {
            return redirect()->back()->with('growl', ['The same rank already exist.', 'danger'])->withInput();
        }
        $data = Rank::find($id);
        $data->save();

        $edit       = RankTranslation::where('rank_id', $id)->where('language_id', getdefaultLanguage()->id)->first();
        $edit->name = $request->name;
        $edit->save();
        return redirect()->route('admin.ranks.index')->with('growl', ['Rank successfully edited.', 'success']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Rank::destroy($id);
    }

}
