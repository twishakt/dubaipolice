<?php

namespace App\Http\Controllers;

use App;

class ApiController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void`
     */
    public function __construct() {
        $this->middleware(['webApi']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function setApiLanguage($param) {

        App::setLocale($param);

        return $param;
    }

    public function getLanguages() {
        $languages = App\Language::OrderBY('name')->get();

        $data = [];
        $i = 0;
        foreach ($languages as $language) {
            $data[$i]['id'] = $language->id;
            $data[$i]['name'] = $language->name;
            $data[$i]['code'] = $language->code;
            $i++;
        }
        return $this->successResponse('', $data);
    }

    public function successResponse($status, $data = '', $toJson = true) {
        $response = [
            "statusCode" => 10000,
            "message" => $status,
        ];
        if (!empty($data)) {
            $response["data"] = $data;
        }
        return $toJson ? response()->json($response) : $response;
    }

    public function errorResponse($status, $errorMessage = null) {
        return response()->json([
                    "statusCode" => 20000,
                    "message" => $status,
                    "errorMessage" => $errorMessage
        ]);
    }

    public function sessionOutResponse($status) {
        return response()->json([
                    "statusCode" => 30000,
                    "message" => $status
        ]);
    }

}
