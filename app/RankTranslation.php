<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RankTranslation extends Model
{
     protected $table = 'rank_translations';

     protected $fillable = [
          'name', 'language_id', 'rank_id'
     ];
}
