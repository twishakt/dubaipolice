<?php

namespace App\Traits;

use Intervention\Image\Facades\Image;
use Storage;
use File;

/**
 * This is a trait use full to store image either in s3 or in local storage
 *
 * @author Shainu
 */
trait ImageTraits {

    private function resize($image, $path, $fileName) {
        $extension = $image->getClientOriginalExtension();
        $imageName = $fileName . rand(1, time()) . '.' . $image->getClientOriginalExtension();

        try {
            if (!env('CDN_ENABLED', false)) {
                $dir = public_path("uploads/" . $path);
                if (!File::isDirectory($dir)) {
                    File::makeDirectory($dir, 0775, true, true);
                }

                $imageRealPath = $image->getRealPath();
                $img = Image::make($imageRealPath);
                $img->save($dir . $imageName);
            } else {
                $dir = env('CDN_FILE_DIR','dev/dreaminn/uploads/') . $path;
                $img = Image::make($image);
                Storage::disk('s3')->put($dir . $imageName, $img->stream()->detach(), 'public');
            }

            return $imageName;
        } catch (Exception $e) {
            return false;
        }
    }

    private function multiImage($image, $path, $fileName, $multiple = true) {
        $extension = $image->getClientOriginalExtension();
        $imageName = $fileName . rand(1, time()) . '.' . $image->getClientOriginalExtension();
        try {
            if (!env('CDN_ENABLED', false)) {
                $dir = public_path("uploads/" . $path);
                if (!File::isDirectory($dir)) {
                    File::makeDirectory($dir, 0775, true, true);
                    File::makeDirectory($dir . 'low/', 0775, true, true);
                    File::makeDirectory($dir . 'high/', 0775, true, true);
                    File::makeDirectory($dir . 'thumb/', 0775, true, true);
                }
                $imageRealPath = $image->getRealPath();

                $img = Image::make($imageRealPath);
                $img->save($dir . $imageName, 100);

                $height = $img->height();
                $width = $img->width();

                if ($multiple) {
                    $img->resize(300, null, function($constraint) {
                        $constraint->aspectRatio();
                    })->save($dir . 'low/' . $imageName);

                    $img->resize(700, null, function($constraint) {
                        $constraint->aspectRatio();
                    })->save($dir . 'high/' . $imageName);

                    $img->resize(100, 100, function($constraint) {
                        //$constraint->aspectRatio();
                    })->save($dir . 'thumb/' . $imageName);
                }
            } else {
                $dir = env('CDN_FILE_DIR','dev/dreaminn/uploads/') . $path;

                $img = Image::make($image);
                Storage::disk('s3')->put($dir . $imageName, $img->stream()->detach(), 'public');

                $height = $img->height();
                $width = $img->width();

                if ($multiple) {
                    $img->resize(300, null, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                    Storage::disk('s3')->put($dir . '/low/' . $imageName, $img->stream()->detach(), 'public');

                    $img->resize(700, null, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                    Storage::disk('s3')->put($dir . '/high/' . $imageName, $img->stream()->detach(), 'public');

                    $img->resize(100, 100, function ($constraint) {
                        //$constraint->aspectRatio();
                    });
                    Storage::disk('s3')->put($dir . '/thumb/' . $imageName, $img->stream()->detach(), 'public');
                }
            }

            return ['name' => $imageName, 'height' => $height, 'width' => $width, 'extn' => $extension];
        } catch (Exception $e) {
            return false;
        }
    }

    private function iconUpload($icon, $path, $fileName) {
         $extension = $icon->getClientOriginalExtension();
         $iconName = $fileName . rand(1, time()) . '.' . $icon->getClientOriginalExtension();
         try {
           if (!env('CDN_ENABLED', false)) {
                $dir = public_path("uploads/" . $path);
                if (!File::isDirectory($dir)) {
                     File::makeDirectory($dir, 0775, true, true);
                }
                $icon->move($dir, $iconName);
           } else {
                $dir = env('CDN_FILE_DIR','dev/dreaminn/uploads/') . $path;
                Storage::disk('s3')->put($dir . $iconName, file_get_contents($icon->getRealPath()), 'public');
           }
           return $iconName;
         } catch (Exception $e) {
           return false;
         }
    }

}
